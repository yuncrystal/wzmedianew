import React, { useState } from "react"
import Box from '@material-ui/core/Box';
import GoogleMapReact from 'google-map-react';
import { FaMapMarkerAlt } from "react-icons/fa";
import emailjs from 'emailjs-com';
import Contact from './contact.png'
import "./contactUs1.scss";
const AnyReactComponent = ({ text }) => (
    <div className='google-place-name'>
        <div> {text}<br /></div>
        <FaMapMarkerAlt size='25px' color='#fc2c25' />
    </div>
);


const ContactUs = () => {

    const defaultProps = {
        center: {
            lat: -33.870713,
            lng: 151.192475
        },
        zoom: 19
    };


    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [detail, setDetail] = useState('')

    const sentEmail = () => {

        const templateParams = {
            email: email,
            username: name,
            phone: phone,
            query_detail: detail
        };
        // emailjs.send('gmail', 'template_U7kmhQED', templateParams, 'user_EawcONlf9jKq6ddEStFxf')
        emailjs.send('sdfghjkl', 'template_lRwnHSJj', templateParams, 'user_ER3G7mBE1aMBXmdtATe1N')
            .then((response) => {
                alert('邮件已发送')
                console.log('SUCCESS!', response.status, response.text);
            }, (err) => {
                console.log('FAILED...', err);
            });
    }

    return (
        <div className='contact'>
            <div className='left'>
                <div className='describe content1 light'>
                    Got a new project or looking for an ongoing design partner? Feel free to get in touch via the form below. Please allow up to 48 hours for us to respond to your message. For anything more urgent, hit up the Smack Bang hot line.
                    </div>
                <div className='address content2'>
                    +61(0) 468 323 526<br /> info@wzm.com.au<br /><br /> 2.04/55 Miller St, Pyrmont NSW 2009
                    </div>
                <form className='pc-show'>
                    <div>
                        <input type="text" placeholder='Name' value={name} onChange={(e) => setName(e.target.value)} />
                    </div>
                    <div>
                        <input type="text" placeholder='Email address' value={email} onChange={(e) => setEmail(e.target.value)} />
                    </div>
                    <div>
                        <input type="text" placeholder='Phone' value={phone} onChange={(e) => setPhone(e.target.value)} />
                    </div>
                    <div>
                        <textarea type="text" placeholder='Enquiry Details' value={detail} onChange={(e) => setDetail(e.target.value)} />
                    </div>
                    <div className='submit content2' onClick={sentEmail}>SEND</div>
                </form>
            </div>

            <div className='right'>
                <div>
                    <img className='pc-show' src={Contact} alt="" />
                </div>
                <div className='google-map'>
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: 'AIzaSyDVBwjz_OWHEl9xQVnin5q5n4t8TkHBrkM' }}
                        // bootstrapURLKeys={{ key: 'AIzaSyDrxVtm4Ddal9Ou9InxbA7mbgKme41CMSQ' }}
                        defaultCenter={defaultProps.center}
                        defaultZoom={defaultProps.zoom}
                        yesIWantToUseGoogleMapApiInternals
                    >
                        <AnyReactComponent
                            lat={-33.870713}
                            lng={151.192475}
                            text="National Parks Association of NSW"
                        />
                    </GoogleMapReact>
                </div>

                <form className='mobile-show'>
                    <div>
                        <input type="text" placeholder='Name' value={name} onChange={(e) => setName(e.target.value)} />
                    </div>
                    <div>
                        <input type="text" placeholder='Email address' value={email} onChange={(e) => setEmail(e.target.value)} />
                    </div>
                    <div>
                        <input type="text" placeholder='Phone' value={phone} onChange={(e) => setPhone(e.target.value)} />
                    </div>
                    <div>
                        <textarea type="text" placeholder='Enquiry Details' value={detail} onChange={(e) => setDetail(e.target.value)} />
                    </div>
                    <div className='submit content2' onClick={sentEmail}>SEND</div>
                </form>
            </div>
        </div>

    )
}


export default ContactUs;