import React from "react"
import Header from '../../componments/headerMarket'
import Footer from '../../componments/footer'
import TitleOne from './title-1.svg'
import TitleTwo from './title-2.svg'
import TitleThree from './title-3.svg'
import TitleFour from './title-4.svg'
import ProjectOne from './project1.png'
import ProjectTwo from './project2.png'
import ProjectThree from './project3.png'
import ProjectFour from './project4.png'
import ProjectFive from './project5.png'

import ProjectOneM from './project1-s.png'
import ProjectTwoM from './project2-s.png'
import ProjectThreeM from './project3-s.png'
import ProjectFourM from './project4-s.png'
import ProjectFiveM from './project5-s.png'

import ServiceOne from './service1.svg'
import ServiceTwo from './service2.svg'
import ServiceThree from './service3.svg'
import ServiceFour from './service4.svg'
import ClientOne from './client1.png'
import ClientTwo from './client2.png'
import ClientThree from './client3.png'
import ClientFour from './client4.png'
import ClientFive from './client5.png'
import ClientSix from './client6.png'
import PartnerOne from './partner1.png'
import PartnerTwo from './partner2.png'
import PartnerThree from './partner3.png'
import PartnerFour from './partner4.png'
import PartnerFive from './partner5.png'
import PartnerSix from './partner6.png'
import PartnerSeven from './partner7.png'
import ProjectHover1 from './project1.svg'
import ProjectHover2 from './project2.svg'
import ProjectHover3 from './project3.svg'
import ProjectHover4 from './project4.svg'
import ProjectHover5 from './project5.svg'
import Slider from "react-slick";
import Touch from '../../componments/contactus'
import './home.scss'


const ProjectSlick = ({ img, imgHover, defaultStyle, title }) => {
    return (
        <div className='mask-block'>
            <div>
                <img src={img} alt="" />
            </div>
            <div className='mask'>
                <div className={defaultStyle}>
                    <img src={imgHover} alt="" />
                </div>
            </div>
            <div className='mask-title'>
                {title}
            </div>
        </div>
    )
}

const settings = {
    dots: true,
    infinite: true,
    adaptiveHeight: true,
    speed: 500,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    className: 'mobile-show2'
};

const HomePage = () => {
    return (
        <div>
            <Header />
            <div className='container about white-bg'>
                <div className='title'>
                    <div>
                        <img src={TitleOne} alt="" />
                    </div>
                </div>
                <div className='content content1 light'>
                    <div><span>WZ Media</span> is a Sydney based Australian Chinese transnational media and culture company founded in 2017. <br /> In 2018, we opened our China branch in NingBo to cover major business areas which include media coverage, marketing, cultural performance and entertainment.</div>
                </div>
            </div>
            <div className='container projects white-bg'>
                <div className='title'>
                    <div>
                        <img src={TitleTwo} alt="" />
                    </div>
                </div>
                <div className='pc-show'>
                    <div className='col-2'>
                        <div className='mask-block'>
                            <div>
                                <img src={ProjectOne} alt="" />
                            </div>
                            <div className='mask'>
                                <div className='mask1'>
                                    <img src={ProjectHover1} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className='mask-block'>
                            <div>
                                <img src={ProjectTwo} alt="" />
                            </div>
                            <div className='mask'>
                                <div className='mask2'>
                                    <img src={ProjectHover2} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-3'>
                        <div className='mask-block'>
                            <div>
                                <img src={ProjectThree} alt="" />
                            </div>
                            <div className='mask'>
                                <div className='mask3'>
                                    <img src={ProjectHover3} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className='mask-block'>
                            <div>
                                <img src={ProjectFour} alt="" />
                            </div>
                            <div className='mask'>
                                <div className='mask4'>
                                    <img src={ProjectHover4} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className='mask-block'>
                            <div>
                                <img src={ProjectFive} alt="" />
                            </div>

                            <div className='mask'>
                                <div className='mask5'>
                                    <img src={ProjectHover5} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <Slider {...settings} >
                    <ProjectSlick img={ProjectOneM} imgHover={ProjectHover1} defaultStyle='mask1' title={'Sing Tour'} />
                    <ProjectSlick img={ProjectTwoM} imgHover={ProjectHover2} defaultStyle='mask2' />
                    <ProjectSlick img={ProjectThreeM} imgHover={ProjectHover3} defaultStyle='mask3' />
                    <ProjectSlick img={ProjectFourM} imgHover={ProjectHover4} defaultStyle='mask4' />
                    <ProjectSlick img={ProjectFiveM} imgHover={ProjectHover5} defaultStyle='mask5' />

                </Slider>


            </div>

            <div className='container services white-bg'>
                <div className='title'>
                    <div>
                        <img src={TitleThree} alt="" />
                    </div>
                </div>
                <div className='content'>
                    <div className='right'>
                        <div>
                            <div>
                                <img src={ServiceOne} alt="" />
                            </div>
                            <div>
                                <img src={ServiceThree} alt="" />
                            </div>

                        </div>
                        <div>
                            <div>
                                <img src={ServiceTwo} alt="" />
                            </div>
                            <div>
                                <img src={ServiceFour} alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className='container clients'>
                <div className='title'>
                    OUR CLIENTS
                </div>

                <div className='album'>
                    <div>
                        <img src={ClientOne} alt="" />
                    </div>
                    <div>
                        <img src={ClientTwo} alt="" />
                    </div>
                    <div>
                        <img src={ClientThree} alt="" />
                    </div>
                    <div>
                        <img src={ClientFour} alt="" />
                    </div>
                    <div>
                        <img src={ClientFive} alt="" />
                    </div>
                    <div>
                        <img src={ClientSix} alt="" />
                    </div>
                </div>
            </div>

            <div className='container partnership'>
                <div className='title'>
                    PARTNERSHIP
                </div>

                <div className='album'>
                    <div>
                        <img src={PartnerOne} alt="" />
                    </div>
                    <div>
                        <img src={PartnerTwo} alt="" />
                    </div>
                    <div>
                        <img src={PartnerThree} alt="" />
                    </div>
                    <div>
                        <img src={PartnerFour} alt="" />
                    </div>
                    <div>
                        <img src={PartnerFive} alt="" />
                    </div>
                    <div>
                        <img src={PartnerSix} alt="" />
                    </div>
                    <div>
                        <img src={PartnerSeven} alt="" />
                    </div>
                </div>
            </div>


            <div className='container touch white-bg'>
                <div className='title'>
                    <div>
                        <img src={TitleFour} alt="" />
                    </div>
                </div>

                <Touch />
            </div>


            <Footer />
        </div>
    )
}

export default HomePage